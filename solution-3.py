class Node:
    def __init__(self, value, parent):
        self.value = value
        self.parent = parent

def path_to_root(node, path_list=None):
    if path_list is None:
        path_list = []

    path_list.append(node.value)
    return path_list if node.parent is None else path_to_root(node.parent, path_list=path_list)

def lca(node1, node2):
    node1_path_to_root = path_to_root(node1)
    while node2 is not None:
        if node2.value in node1_path_to_root:
            return node2.value
        node2 = node2.parent

    return -1  # when node2 is not present in the tree

"""
Runtime: O(N)
Memory requirements: O(N)

For both the steps of storing and checking for a match in ancestor,
this algorithm takes O(h), where h is the distance from leaf to root.

Since it is storing the path from the first node to root, in the worst
case it has to store h values.

For a very unbalanced tree where every node of the tree has exactly 1
child, the tree becomes linear and in that case h=N, where N is the total
number of nodes in the tree. This makes the runtime and memory
requirements both to be O(N).
"""


# Testing
import unittest


class TestLCA(unittest.TestCase):

    def setUp(self):
        self.error_msg = 'Wrong Output'
        self.node1 = Node(1, None)
        self.node2 = Node(2, self.node1)
        self.node3 = Node(3, self.node1)
        self.node4 = Node(4, self.node2)
        self.node5 = Node(5, self.node2)
        self.node6 = Node(6, self.node3)
        self.node7 = Node(7, self.node3)
        self.node8 = Node(8, self.node4)
        self.node9 = Node(9, self.node4)
        self.node10 = Node(10, self.node4)

    def test_lca(self):
        self.assertEqual(lca(self.node3, self.node7), 3, msg=self.error_msg)
        self.assertEqual(lca(self.node6, self.node7), 3, msg=self.error_msg)
        self.assertEqual(lca(self.node4, self.node3), 1, msg=self.error_msg)
        self.assertEqual(lca(self.node8, self.node4), 4, msg=self.error_msg)
        self.assertEqual(lca(self.node8, self.node5), 2, msg=self.error_msg)
        self.assertEqual(lca(self.node5, self.node6), 1, msg=self.error_msg)
        self.assertEqual(lca(self.node5, self.node4), 2, msg=self.error_msg)
        self.assertEqual(lca(self.node1, self.node1), 1, msg=self.error_msg)
        self.assertEqual(lca(self.node1, self.node2), 1, msg=self.error_msg)
        self.assertEqual(lca(self.node9, self.node4), 4, msg=self.error_msg)
        self.assertEqual(lca(self.node4, self.node5), 2, msg=self.error_msg)


if __name__ == '__main__':
    unittest.main()
