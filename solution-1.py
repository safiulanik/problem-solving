def print_depth(data, level=1):
    for key, value in data.items():
        print("{} {}".format(key, level))
        if isinstance(value, dict):
            print_depth(value, level + 1)


# Testing
import unittest
from io import StringIO
from unittest.mock import patch


class TestPrintDepthV2(unittest.TestCase):

    def setUp(self):
        self.error_msg = 'Wrong Output'
        self.input = {
            'key1': 1,
            'key2': {
                'key3': 1,
                'key4': {
                    'key5': 4
                }
            }
        }
        self.expected_output = """key1 1
key2 1
key3 2
key4 2
key5 3
"""

    def test_print_depth(self):
        with patch('sys.stdout', new=StringIO()) as fake_out:
            print_depth(self.input)
            self.assertEqual(fake_out.getvalue(), self.expected_output, self.error_msg)

if __name__ == '__main__':
    unittest.main()
