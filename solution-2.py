def print_depth(data, level=1):
    for key, value in data.items():
        print("{} {}".format(key, level))
        if isinstance(value, dict):
            print_depth(value, level + 1)
        elif isinstance(value, Person):
            print_depth(value.__dict__, level + 1)
        else:
            continue  # not required, added for enhanced readability

class Person(object):
    def __init__(self, first_name, last_name, father):
        self.first_name = first_name
        self.last_name = last_name
        self.father = father


# Testing
import unittest
from io import StringIO
from unittest.mock import patch


class TestPrintDepth(unittest.TestCase):

    def setUp(self):
        self.error_msg = 'Wrong Output'
        self.person_a = Person('User', '1', None)
        self.person_b = Person('User', '2', self.person_a)

        self.input = {
            'key1': 1,
            'key2': {
                'key3': 1,
                'key4': {
                    'key5': 4,
                    'user': self.person_b,
                }
            },
        }
        self.expected_output = """key1 1
key2 1
key3 2
key4 2
key5 3
user 3
first_name 4
last_name 4
father 4
first_name 5
last_name 5
father 5
"""

    def test_print_depth(self):
        with patch('sys.stdout', new=StringIO()) as fake_out:
            print_depth(self.input)
            self.assertEqual(fake_out.getvalue(), self.expected_output, self.error_msg)

if __name__ == '__main__':
    unittest.main()
